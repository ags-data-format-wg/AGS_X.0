#Introduction

# Press Release - January 2018

##AGS Data Format Registered Users

The AGS publication 'Electronic Transfer of Geotechnical and Geoenvironmental Data' (AGS data format versions 1-4.0) has been maintained on behalf of the industry for the last 25 years using a one-time fee for registered users.

As the AGS Format's use has grown, the process of responding to queries, extending the format and maintaining the website has become more costly and time consuming and this has to be addressed. Whilst the publication itself will still be completely free to download and use, for those wishing to make themselves familiar with its content. However, the AGS has decided that an annual fee should be payable as a contribution to its upkeep for those organizations sending or receiving AGS files.

This will come into effect on 1st January 2018 with the annual fee for AGS member organizations set at £50 and non-member organizations £150.

From April 2018, the existing list of Registered Companies will be cleared and only those organizations and individuals who have paid the fee for that year will be included in the revised list. A graphic and certificate will be supplied annually by AGS to those who are registered for inclusion on their website, email and social media feeds to demonstrate their registration and involvement. We would ask that, if you are a consumer of AGS data, you check that any company supplying you with AGS data is shown on the list of Registered Users. 

Jackie Bland

Working Party Chairman

---

# Press Release - March 2017

##AGS 4.0.4

In response to new laboratory industry standards and updated UK practice the "AGS4 Addendum October 2011" document has been updated and amended and shall be known as "AGS 4.0.4".

Detailed amendments are listed in Appendix 3 but summarised below:

- The term 'Moisture content' has been replaced with the term 'Water content' in accordance with BS EN ISO 17892-1:2014.
- PMTD_SEQ heading in the'Pressuremeter Test Results - Individual Loops' (PMTL) group is marked for deletion.
- New headings are GCHM_DLM, GCHM_RTXT, LOCA_NATD, LOCA_ORID, LOCA_ORJO, LOCA_ORCO, SAMP_RECL and RDEN_IDEN.
- Data types have been changed for CHOC_REF, FRAC_FI, FRAC_IAVE, FRAC_IMAX, IPID_RES, LVAN_VNPK, LVAN_VNRM, RPLT_PLS, RPLT_PLSI, TRIT_CU.
- Window Sampling amended to Dynamic sampling throughout document.
- Under section 8 Rules, item 8.3 Group Hierarchy, there are 10 groups that are not part of the parent-child hierarchy - PREM was previously missing from the list and has now been included.

The newly defined headings should be viewed as user defined headings and thus, if used, the DICT group needs to be present in every AGS 4.0.4 submission.
All current abbreviations are applicable to AGS 4.0.4.

Jackie Bland

Working Party Chairman

---

# Press Release - October 2011

##AGS4 Addendum 3

Further areas of the document have required updating. We have prepared an amended version of AGS4 to be known as "AGS4 Addendum 3", which wholly replaces “AGS4”.

Major changes from initial release in May 2010 to October 2011 include:

- ????_TESN heading added to each laboratory testing group
- CMPG_TESN added to CMPG and CMPT as key heading
- ????_REM added to CMPT, DETL, FRAC, GEOL, GRAT, MCVT, PLTT, PMTL, SCDT, TRET, TRIT groups
- CHOC_REF changed to a key field
- DICT_UNIT data type corrected to PU
- Exploratory hole orientation and inclination group (HORN) added
- 'Natural Moisture Content Test' group (LNMC) title amended to 'Moisture Content Test'. LNMC_ISNT heading added to indicate whether the test result has been assumed to be a natural moisture content.
- Monitoring Readings group (MOND) expanded to incorporate detection limits, measurement method and accreditation
- Appendix 3 has been removed from this publication in favour of a more easily updated Guidance Document on the website.
- Ø characters in heading names corrected to 0 characters.
- IPID_RES suggested type changed to 2DP.
- LRES_WCND changed to LRES_WRES to express results as water resistivity in ohm m. LSLT_MCI added.
- Heading PMTL_RRM corrected to PMTL_REM (PMTL group).
- PTST_TYPE type changed to PA and example updated.

Jackie Bland
Working Party Chairman

---

# Press Release - May 2011

##AGS4 Addendum 2

The remit of the Working Party is to monitor the use of the AGS Format for the electronic transfer of data in the geotechnical and geoenvironmental industries. 

After consultation with developers and other interested parties, we have identified some areas of AGS4, released in May 2010, which need updating. To this end, we have prepared an amended version of AGS4 to be known as "AGS4 Addendum", which wholly replaces AGS4. 

Major changes include: 

- _TESN heading added to each laboratory testing group
- _REM added to CMPT, DETL, FRAC, GEOL, GRAT, MCVT, PLTT, SCDT, TRET, TRIT groups
- CHOC_REF changed to a key field
- DICT_UNIT data type corrected to PU
- Exploratory hole orientation and inclination group (HORN) added
- 'Natural Moisture Content Test' group (LNMC) title amended to 'Moisture Content Test'. LNMC_ISNT heading added to indicate whether the test result has been assumed to be a natural moisture content.
- Monitoring Readings group (MOND) expanded to incorporate detection limits, measurement method and accreditation
- Appendix 3 has been removed from this publication in favour of a more easily updated Guidance Document on the website.

With a view to providing more general guidance in specific situations we have developed a series of Guidance Documents which are held on the website. These documents will be revised and added to when the need arises. 

Jackie Bland

Working Party Chairman

---

# THE AGS 4 FILE FORMAT - Published May 2010

The AGS Data Management Working Party continually monitors the use of the AGS Format for the electronic transfer of data in the geotechnical and geoenvironmental industries and has prepared a new version to be known as "AGS4" which contains an updated Data Dictionary and revised rules for AGS Format files. 
Whilst the AGS Format is used throughout the world, this document is specifically written for use in accordance with UK practice. Guidance notes for its use with other codes and standards will be available. 

The Data Dictionary within AGS4 contains the list of data items which can now be transferred. This has been extended from AGS3 to include items required by Eurocodes and associated amendments of British Standards, together with the information necessary for accreditation of test results by external bodies and typical Quality Assurance schemes. 

The organisation of the laboratory test results have been restructured to provide separate groups for each test, using paired groups where appropriate. 

A new group, GCHM has been introduced for the results of geotechnical chemical testing relating to the aggressivity of the ground to concrete in accordance with BRE Special Digest 1. 
Rock testing has been divided into aggregate and geotechnical tests. 

A new group, ERES, has been added for the test results of environmental samples replacing CNMT / ICCT. This group extends the level of detail that can be transferred for each chemical test result. 
Within the Data Dictionary an additional key field has been added to the sample group to be known as SAMP_ID, (unique identifier) which will enable the use of a single identifier for samples to be used where this is appropriate. This is particularly relevant for geoenvironmental studies when taking monitoring or control samples and also facilitates the use of bar-coded samples. 

Groups for Chain of Custody and Scheduling of Laboratory Testing have been added which extend the format from use as a report deliverable to a mechanism to support the process of ground investigation and testing. 

The rules for the writing of the transfer file have been revised with the removal of the 240 character line restriction erasing the need for the line; a feature of previous AGS Formats. The addition of line headers (Data Descriptors) clarifies the format even further. The CSV format has been retained rather than move to XML, as this is being investigated by others and there seems to be no commercial advantage to changing to XML at this time for the UK geotechnical industry. 
Guidance notes on the application of each data group are provided within the Data Dictionary. 

Steve Wathall

Working Party Chairman