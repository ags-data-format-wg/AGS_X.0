The table below shows costs and areas of the AGS Data Format website accessible to each type of user, register [here](https://www.ags.org.uk/data-format/data-format-registration/)

Permission|Anonymous*|AGS Website account only*|Individual (Non AGS member)|Student|Open Source Software Developer|Commercial Software Developer|AGS Member Organisation|Company (Non AGS Member)
:-------|:--------:|:--------:|:--------:|:--------:|:--------:|:-------:|:--------:|:--------:
Cost                                        |£0 |£0 |£60 |£0 |£0 |£500 |£50 |£150
News                                        |y |y |y |y |y |y |y |y
View 'Data Format Resistered List'          |y |y |y |y |y |y |y |y
Download Format                             |&nbsp; |y |y |y |y |y |y |y
View Forum                                  |&nbsp; |y |y |y |y |y |y |y
Forum Participation                         |&nbsp; |&nbsp; |y |y |y |y |y |y
Data Dictionary and abbreviation lists      |&nbsp; |&nbsp; |y |y |y |y |y |y
Name shown on 'Data Format Registered List' |&nbsp; |&nbsp; |y |&nbsp; |&nbsp; |y |y |y
Name shown on 'Software List'               |&nbsp; |&nbsp; |&nbsp; |&nbsp; |&nbsp;|y |&nbsp; |&nbsp;
Access to early release and support         |&nbsp; |&nbsp; |&nbsp; |&nbsp; |&nbsp; |y |&nbsp; |&nbsp;

* Data Format registration not required
