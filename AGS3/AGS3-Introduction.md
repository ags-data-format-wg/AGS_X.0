#Introduction

#Press Release - March 2018

##AGS 3.0/3.1 Retirement

**To: AGS Data Format User**

An earlier version of the digital data transfer format used to transfer ground investigation, laboratory testing and monitoring data rapidly between contributing parties to a geotechnical or geoenvironmental project will cease to be supported from 8th March 2018.

Version 3.0/3.1 of the Association of Geotechnical and Geoenvironmental Specialists (AGS) digital data transfer format, the so called ‘AGS Format’, was published in March 2005 and has been widely adopted within the ground engineering industry.

In May 2010 version 4 of the AGS Format was published, and has been gaining users and popularity since then. The latest 4.0.4 update was released in February 2017 in response to new laboratory industry standards and updated UK practice, and contains many features that are not available in the old 3.1 version.

The maintenance and updating of the AGS Format is carried out by a working group of volunteers. The decision to retire AGS 3.1 has been taken in order to allow the working group more time for future developments, and to speed up the response times to queries posted on the [website](https://www.ags.org.uk/data-format/).

Jackie Bland, Leader of the AGS Data Management Working Group said:

> This does not mean that industry colleagues will have to stop using the AGS 3.1 format, but it simply means that the working group will no longer respond to queries on 3.1 or its associated codes.

> This should free up time to provide a swifter response to queries related to AGS4, which should now be the go-to choice for the efficient transfer of ground investigation, laboratory testing and monitoring data.”


#Press Release - May 2005

##AGS 3.1 Publication Update

- The "Electronic Transfer of Geotechnical and Geoenvironmental Data Edition 3.1 " publication has been updated with post publication corrections.
- ?ICCT - removed key field markings against ?ICCT_UNIT and ?ICCT_METH and added ?ICCT_ULIM to maintain compatibility with CNMT group, page 34.
- ?IFID - added ?IFID_TESN as key field and added ?GEOL_STAT to maintain consistency with other in situ test data groups, page 35.
- ?IPID - added ?IFID_TESN as key field and added ?GEOL_STAT to maintain consistency with other in situ test data groups, page 37.
- ppmv - parts per million volume used in example of ?IFID_RES added to UNITS listing in Appendix 1.
- Change to description of HOLE_ID throughout document marked as Rev.
- Revised examples for ?BKFL_LEG, ?CBRT_REM, ?CLSS_REM, CONG_REM and ?TRIX_CU.
- Added missing Del flags to deleted fields in INST and STCN.
- Removed reference to a 'SPEC_TYPE' field in Appendix 6 discussion on user defined abbreviations (Section 7).
- Removed duplicate entries for HCARS and PAHS from CODE listing in Appendix 1.
- Amended Appendix footers to AGS Edition 3.1
- Corrected spelling errors on ABBR table.

An updated version of the publication can be download from this Website.

---

#Press Release - December 2004

The AGS Data Format committee wishes to announce the publication of Revision 1 to the third edition of the AGS Data transfer format.

The AGS Data Format subcommittee has monitored the use of the format within the industry since the launch of AGS 3 in 1999. The committee considers that it is now appropriate to issue AGS 3.1 to include the developments which have occurred over the last few years. In accordance with section 9 of the AGS 3 publication the majority of this document includes format additions requested by the industry.

There are no major changes from AGS 3 and therefore the committee have decided to call this Revision 1 of the AGS 3 format (AGS 3.1) rather than AGS 4. The changes in this revision are new fields, groups and pick list items all sitting within the AGS 3 framework.

AGS 3.1 is compliant with the rules in AGS 3 and therefore the ? remains in all new headings and groups even though these are now in common use.

This revision brings together AGS 3, the “The AGS-M Format - for the electronic transfer of monitoring data “ published by AGS and CIRIA in 2002 and other groups and headings, which have been suggested on the AGS website and used by the industry.

A complete list of all additions, revisions and their history is available on the AGS web pages ([http://www.agsdataformat.com](http://www.agsdataformat.com)) The AGS data format website has been updated to display all the additions in this document together will the appropriate guidance notes. The website also allows the visitor to view the field version history and an appropriate discussion threads that have contributed to the changes

It is expected that all registered users will be able to use these additional headings for projects starting after March 2005 but this is solely up to the relevant project members to agree.

---

#Press Release - September 2004

The Association of Geotechnical and Geoenvironmental Specialists announce a project to take the AGS data transfer format into the future.
Much work has been done, both in UK and overseas, to research ways to bring the AGS interchange system more in line with Extensible Mark-up Language (XML) - the global standard for data transfer in web-based communications. However, the overriding responsibility of the Association is the protection of its members' interests, so any solution would need to preserve:

1.  compatibility with established practices,
2. the ability to use data compiled in earlier editions, and
3. the independence of the format from particular software environments.... more

---

#The Underlying Philosophy

The purpose of the AGS Format is to provide a means of transferring geotechnical and geoenvironmental data between parties. From the outset the fundamental consideration has been that potential users of the Format should be able to use standard software tools to produce the data file. These tools may range from simple text editors and word processors, through spreadsheet packages to sophisticated database systems. In order to ensure the widest possible level of acceptance it was also agreed that the Format should use the American Standard Code for Information Interchange (ASCII).

To minimise file size and eliminate potential conflicts it was also considered that the data file should only contain fundamental data such as exploratory hole and test data required to be reported by the relevant British (or other National) Standard or similar recognised documents. Interpreted or derived data should not be transmitted – any further processing being at the discretion, and under the control of the receiving user.

To provide maximum flexibility and to enable hardcopy printout for checking or other purposes, a "data dictionary" approach as first proposed by Greenwood (1988) has been utilised as the foundation for the Format. This concept is both compatible with the needs of software developers and simple to implement.

##File Format

The AGS Format file is a text file, containing tables of information on all aspects of the geotechnical site investigation. Each table comprises a Group name, column Headings and data Variables as illustrated in the example

##Group Names

The AGS data book, AGS(1992), AGS(1994), contains all the tables required for a site investigation. The group name is shortened to a four-letter code, often the first letters of the table description. Group names must be on a line by themselves and surrounded by quotes. A list of AGS(1994) Group names is included in Appendix A.

##Column Headings

The column headings (or field names) must be listed below the group name line as shown in Figure 10. Every column must be surrounded by quotes and separated by a comma thus allowing programs to determine where the start and end of each data item is. Each header is abbreviated to a code listed in the AGS data book.

The column header codes allowed for the hole table are shown in Figure 11, only the codes marked with a "*" are mandatory within a table. The hole group in Figure 10 contains 8 column headers.

##Data Variables

The third section of the hole table actually contains the data for all the holes in this project. Each column must again be surrounded by quotes and separated by a comma.

##Flexibility

The file structure described above, with its use of the data dictionary concept and subdivision into small groups, allows a high degree of flexibility. New groups can readily be added without disturbing existing groups, which is particularly useful in the international context, allowing local requirements to be incorporated. Similarly, fields can be added to existing groups to take account of particular needs as appropriate.
